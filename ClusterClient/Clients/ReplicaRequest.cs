﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace ClusterClient.Clients
{
    enum RequestState
    {
        WaitingToRun,
        Running,
        RanToCompletion,
        Faulted,
        Cancelled
    }

    class ReplicaRequestException : AggregateException
    {
        public ReplicaRequestException(string message, Exception innerException) : base(message, innerException) { }
    }

    class ReplicaRequest
    {
        public readonly string ReplicaUri;
        private readonly HttpWebRequest dataRequest;
        private int? queryId;

        public int? Workload { get; private set; }

        public DateTime? StartTime { get; private set; }

        public RequestState State { get; private set; }

        public ReplicaRequest(string replicaUri, string query)
        {
            ReplicaUri = replicaUri;
            dataRequest = query != null ? CreateRequest(replicaUri + "?query=" + query) : null;
        }

        private static HttpWebRequest CreateRequest(string uri)
        {
            var request = WebRequest.CreateHttp(Uri.EscapeUriString(uri));
            request.Proxy = null;
            request.KeepAlive = true;
            request.ServicePoint.UseNagleAlgorithm = false;
            request.ServicePoint.ConnectionLimit = 100500;
            return request;
        }

        public async Task<string> Perform()
        {
            if (State != RequestState.WaitingToRun)
                throw new InvalidOperationException("ReplicaRequest.Perform should be called once");
            StartTime = DateTime.Now;
            State = RequestState.Running;

            try
            {
                using (var response = await dataRequest.GetResponseAsync())
                {
                    // ReSharper disable once AssignNullToNotNullAttribute
                    var reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                    queryId = int.Parse(await reader.ReadLineAsync());

                    var result = await reader.ReadToEndAsync();

                    State = RequestState.RanToCompletion;
                    Log.InfoFormat("Response from {0} received in {1} ms",
                        dataRequest.RequestUri, DateTime.Now.Subtract(StartTime.Value).TotalMilliseconds);
                    return result;
                }
            }
            catch (Exception e)
            {
                State = RequestState.Faulted;
                throw new ReplicaRequestException("Failed to perform a request", e);
            }
        }

        public void Cancel()
        {
            if (State == RequestState.WaitingToRun)
                throw new InvalidOperationException("Cancelling a task that didn't start to perform");
            if (State != RequestState.Running)
                return;
            State = RequestState.Cancelled;

            dataRequest.Abort();

            if (queryId == null)
                return;
            Task.Run(() => CreateRequest(ReplicaUri + "?action=cancel&id=" + queryId).GetResponse());
        }

        public async Task GetWorkload()
        {
            var request = CreateRequest(ReplicaUri + "?action=get_workload");
            using (var response = await request.GetResponseAsync())
            {
                // ReSharper disable once AssignNullToNotNullAttribute
                var reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                Workload = int.Parse(await reader.ReadToEndAsync());
            }
        }

        private static readonly ILog Log = LogManager.GetLogger(typeof(ReplicaRequest));
    }

}
