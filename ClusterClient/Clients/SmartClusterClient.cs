﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ClusterClient.Clients
{
    class SmartClusterClient : ClusterClientBase
    {
        private readonly ReplicaStats stats;
        private readonly SemaphoreSlim replicaSelectionSemaphore;

        public SmartClusterClient(string[] replicaAddresses)
            : base(replicaAddresses)
        {
            stats = new ReplicaStats(replicaAddresses);
            replicaSelectionSemaphore = new SemaphoreSlim(1);
        }

        protected virtual Task<string[]> GetBestReplicas(TimeSpan totalTimeout)
        {
            return Task.FromResult(stats.GetBestReplicas());
        }

        public override async Task<string> RequestAsync(string query, TimeSpan timeout)
        {
            var finishTime = DateTime.Now + timeout;

            var noRequestMade = true;
            await replicaSelectionSemaphore.WaitAsync();
            // Без блокировки все параллельно запущенные RequestAsync выберут один и тот же сервер и перегрузят его своими запросами.
            // Блокировка позволяет выбирать новый сервер только после того, как послан запрос на выбранный до этого сервер.

            var bestReplicas = await GetBestReplicas(timeout);
            Log.InfoFormat("bestReplicas[0] = {0}", bestReplicas[0]);

            var requests = new List<ReplicaRequest>();
            var awaitedTasks = new HashSet<Task<string>>();
            try
            {
                foreach (var uri in bestReplicas)
                {
                    var req = new ReplicaRequest(uri, query);
                    stats.AddStartedRequest(req);
                    requests.Add(req);
                    awaitedTasks.Add(req.Perform());

                    if (noRequestMade)
                    {
                        noRequestMade = false;
                        replicaSelectionSemaphore.Release();
                    }
                    
                    var remainingTime = finishTime.Subtract(DateTime.Now);
                    var timeoutPerReplica = TimeSpan.FromMilliseconds(remainingTime.TotalMilliseconds / bestReplicas.Length);
                    try
                    {
                        return await awaitedTasks.WaitForResult(timeoutPerReplica);
                    }
                    catch (AggregateException) { }
                    catch (TimeoutException) { }
                }

                throw new TimeoutException("No accessible replicas found");
            }
            finally
            {
                foreach (var req in requests)
                {
                    req.Cancel();
                    stats.AddFinishedRequest(req);
                }
            }
        }
    }
}
