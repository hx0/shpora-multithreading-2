﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ClusterClient.Clients
{
    class WorkloadAwareClusterClient : SmartClusterClient
    {
        public WorkloadAwareClusterClient(string[] replicaAddresses)
            : base(replicaAddresses) { }

        const double WorkloadTimeoutCoeff = 0.1;

        protected override async Task<string[]> GetBestReplicas(TimeSpan totalTimeout)
        {
            var timer = Stopwatch.StartNew();

            var workloadGettingTimeout = TimeSpan.FromMilliseconds(totalTimeout.TotalMilliseconds * WorkloadTimeoutCoeff);

            var requests = ReplicaAddresses
                .Select(uri => new ReplicaRequest(uri, null))
                .ToArray();
            try
            {
                await Task.WhenAll(requests.Select(req => req.GetWorkload()))
                    .WaitFor(workloadGettingTimeout);
            }
            catch (TimeoutException) { }

            var replicas = requests
                .Where(req => req.Workload != null)
                .OrderBy(req => req.Workload.Value)
                .Select(req => req.ReplicaUri)
                .ToArray();
            
            Log.InfoFormat("Workload: average {0} on {1} replicas",
                // ReSharper disable once PossibleInvalidOperationException
                (double)requests.Sum(req => req.Workload.Value) / requests.Length, requests.Length);

            if (replicas.Length == 0)
                throw new TimeoutException("No replicas seem working");
            return replicas;
        }
    }
}
