﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ClusterClient.Clients
{
    class ReplicaStats
    {
        private readonly Random random;
        private readonly Dictionary<string, ReplicaStatsItem> replicas;

        public ReplicaStats(string[] replicaAdresses)
        {
            random = new Random();
            replicas = new Dictionary<string, ReplicaStatsItem>();
            foreach (var uri in replicaAdresses)
                replicas[uri] = new ReplicaStatsItem();
        }

        public void AddStartedRequest(ReplicaRequest request)
        {
            lock (replicas)
                replicas[request.ReplicaUri].AddStartedRequest();
        }

        public void AddFinishedRequest(ReplicaRequest request)
        {
            lock (replicas)
                replicas[request.ReplicaUri].AddFinishedRequest(request);
        }

        public string[] GetBestReplicas()
        {
            lock (replicas)
            {
                var result = random.Shuffle(replicas)
                    .Where(pair => !pair.Value.InGrayList)
                    .OrderBy(pair => pair.Value.ExpectedQueryDuration)
                    .Select(pair => pair.Key)
                    .ToArray();
                if (result.Length == 0)
                    throw new TimeoutException("All replicas are overloaded now");
                return result;
            }
        }
    }
}
