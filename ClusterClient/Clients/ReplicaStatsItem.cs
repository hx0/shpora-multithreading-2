﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace ClusterClient.Clients
{
    class ReplicaStatsItem
    {
        private readonly List<double> queryDurations;
        private int workload;

        public ReplicaStatsItem()
        {
            queryDurations = new List<double>();
            workload = 0;
        }

        private static readonly TimeSpan TimeoutToPlaceToGrayList = TimeSpan.FromSeconds(2);
        private static readonly TimeSpan GrayListPlacementTimeout = TimeSpan.FromSeconds(2);

        private DateTime? graylistPlacementTime;

        public void AddStartedRequest()
        {
            workload++;
        }

        public void AddFinishedRequest(ReplicaRequest request)
        {
            if (request.StartTime == null)
                throw new ArgumentException("Adding a non-started request");

            workload--;
            if (workload < 0)
                throw new InvalidOperationException("More finished requests than started requests");

            var curDuration = DateTime.Now - request.StartTime.Value;
            if (request.State == RequestState.RanToCompletion)
                queryDurations.Add(curDuration.TotalMilliseconds);
            else
            if (request.State == RequestState.Faulted || (request.State == RequestState.Cancelled &&
                    curDuration >= TimeoutToPlaceToGrayList))
                graylistPlacementTime = DateTime.Now;
        }

        public bool InGrayList => graylistPlacementTime != null &&
                                  DateTime.Now.Subtract(graylistPlacementTime.Value) <= GrayListPlacementTimeout;
        
        const int DefaultQueryDuration = 200;

        public double? ExpectedQueryDuration
        {
            get
            {
                var averageDuration = queryDurations.Count == 0 ? queryDurations.Sum() / queryDurations.Count : DefaultQueryDuration;
                return averageDuration * (1 + workload);
            }
        }
    }
}
