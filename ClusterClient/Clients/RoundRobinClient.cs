﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ClusterClient.Clients
{
    class RoundRobinClient : ClusterClientBase
    {
        private readonly ReplicaStats stats;
        private readonly SemaphoreSlim replicaSelectionSemaphore;

        public RoundRobinClient(string[] replicaAddresses)
            : base(replicaAddresses)
        {
            stats = new ReplicaStats(replicaAddresses);
            replicaSelectionSemaphore = new SemaphoreSlim(1);
        }

        public override async Task<string> RequestAsync(string query, TimeSpan timeout)
        {
            var finishTime = DateTime.Now + timeout;

            var noRequestMade = true;
            await replicaSelectionSemaphore.WaitAsync();

            var bestReplicas = stats.GetBestReplicas();
            Log.InfoFormat("bestReplicas[0] = {0}", bestReplicas[0]);
            
            foreach (var uri in bestReplicas)
            {
                var request = new ReplicaRequest(uri, query);
                stats.AddStartedRequest(request);
                var task = request.Perform();

                if (noRequestMade)
                {
                    noRequestMade = false;
                    replicaSelectionSemaphore.Release();
                }

                var remainingTime = finishTime.Subtract(DateTime.Now);
                var timeoutPerReplica = TimeSpan.FromMilliseconds(remainingTime.TotalMilliseconds / bestReplicas.Length);
                try
                {
                    return await task.WaitFor(timeoutPerReplica);
                }
                catch (ReplicaRequestException) { }
                catch (TimeoutException)
                {
                    request.Cancel();
                }
                finally
                {
                    stats.AddFinishedRequest(request);
                }
            }

            throw new TimeoutException("No accessible replicas found");
        }
    }
}
