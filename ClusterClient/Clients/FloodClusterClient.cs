﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ClusterClient.Clients
{
    class FloodClusterClient : ClusterClientBase
    {
        public FloodClusterClient(string[] replicaAddresses)
            : base(replicaAddresses) { }

        public override async Task<string> RequestAsync(string query, TimeSpan timeout)
        {
            var requests = ReplicaAddresses.Select(uri => new ReplicaRequest(uri, query)).ToArray();
            var awaitedTasks = new HashSet<Task<string>>(requests.Select(req => req.Perform()));
            try
            {
                return await awaitedTasks.WaitForResult(timeout);
            }
            catch (AggregateException)
            {
                throw new TimeoutException("No accessible replicas found");
            }
            finally
            {
                foreach (var req in requests)
                    req.Cancel();
            }
        }
    }
}
