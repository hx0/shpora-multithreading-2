﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ClusterClient.Clients
{
    public class RandomClusterClient : ClusterClientBase
    {
        private readonly Random random = new Random();

        public RandomClusterClient(string[] replicaAddresses)
            : base(replicaAddresses) { }

        const int CooldownTimeout = 100;

        void Cooldown(DateTime finishTime)
        {
            var remainingTime = finishTime.Subtract(DateTime.Now);
            if (remainingTime.TotalMilliseconds <= CooldownTimeout)
                throw new TimeoutException("No time to make one more request");
            Thread.Sleep(CooldownTimeout);
        }

        public override async Task<string> RequestAsync(string query, TimeSpan timeout)
        {
            var finishTime = DateTime.Now + timeout;
            while (true)
            {
                var uri = ReplicaAddresses[random.Next(ReplicaAddresses.Length)];
                var request = new ReplicaRequest(uri, query);
                try
                {
                    return await request.Perform().WaitFor(finishTime.Subtract(DateTime.Now));
                }
                catch (ReplicaRequestException)
                {
                    // If an exception thrown before the timeout ended, let's try to request again
                    if (ReplicaAddresses.Length == 1)
                        // Avoid constant hammering of one server
                        Cooldown(finishTime);
                }
                catch (TimeoutException)
                {
                    request.Cancel();
                    throw;
                }
            }
        }
    }
}