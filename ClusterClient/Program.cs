﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClusterClient.Clients;
using Fclp;
using log4net;
using log4net.Config;

namespace ClusterClient
{
    static class Program
    {
        private static readonly string[] Queries =
        {
            "От", "топота", "копыт", "пыль", "по", "полю", "летит", "На",
            "дворе", "трава", "на", "траве", "дрова"
        };

        const int IterationsPerClient = 10;

        static void Main(string[] args)
        {
            XmlConfigurator.Configure();

            string[] replicaAddresses;
            if (!TryGetReplicaAddresses(args, out replicaAddresses))
                return;

            var clients = new ClusterClientBase[]
            {
                new RandomClusterClient(replicaAddresses),
                new FloodClusterClient(replicaAddresses),
                new RoundRobinClient(replicaAddresses),
                new SmartClusterClient(replicaAddresses),
                new WorkloadAwareClusterClient(replicaAddresses), 
            };

            var stats = new Dictionary<ClusterClientBase, List<long>>();
            try
            {
                foreach (var client in clients)
                {
                    var elapsedTime = new List<long>();
                    for (var i = 0; i < IterationsPerClient; i++)
                    {
                        Console.Write($"{client.GetType().Name}, iteration #{i + 1}... ");
                        Console.Out.Flush();
                        var curElapsedTime = TestClient(client, Queries);
                        elapsedTime.Add(curElapsedTime);
                        Console.WriteLine($"{curElapsedTime} ms");
                    }
                    stats[client] = elapsedTime;

                    Console.WriteLine();
                }

                Console.WriteLine("Statistics (average):");
                foreach (var item in stats)
                    Console.WriteLine($"{item.Key.GetType().Name} -> {item.Value.Sum() / item.Value.Count} ms");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Client error: {e.Message}");
                Log.Fatal(e);
            }
        }

        private static async Task RunQuery(ClusterClientBase client, string query, Stopwatch timer)
        {
            try
            {
                var result = await client.RequestAsync(query, TimeSpan.FromSeconds(6));

                Log.DebugFormat("Processed query \"{0}\" in {1} ms: {2}",
                        query, timer.ElapsedMilliseconds, result);
            }
            catch (TimeoutException)
            {
                Log.Warn($"Query \"{query}\" timeout ({timer.ElapsedMilliseconds} ms)");

                Console.Write("timeout ");
                Console.Out.Flush();
            }
        }

        private static long TestClient(ClusterClientBase client, string[] queries)
        {
            Log.DebugFormat("Testing {0} started", client.GetType());

            var timer = Stopwatch.StartNew();
            Task.WaitAll(queries.Select(async query => await RunQuery(client, query, timer)).ToArray());

            Log.DebugFormat("Testing {0} finished (total {1:0} ms)", client.GetType(), timer.ElapsedMilliseconds);
            return timer.ElapsedMilliseconds;
        }

        private static bool TryGetReplicaAddresses(string[] args, out string[] replicaAddresses)
        {
            var argumentsParser = new FluentCommandLineParser();
            string[] result = {};

            argumentsParser.Setup<string>('f', "file")
                .WithDescription("Path to the file with replica addresses")
                .Callback(fileName => result = File.ReadAllLines(fileName))
                .Required();

            argumentsParser.SetupHelp("?", "h", "help")
                .Callback(text => Console.WriteLine(text));

            var parsingResult = argumentsParser.Parse(args);

            if (parsingResult.HasErrors)
            {
                argumentsParser.HelpOption.ShowHelp(argumentsParser.Options);
                replicaAddresses = null;
                return false;
            }

            replicaAddresses = result;
            return !parsingResult.HasErrors;
        }

        private static readonly ILog Log = LogManager.GetLogger(typeof(Program));
    }
}
