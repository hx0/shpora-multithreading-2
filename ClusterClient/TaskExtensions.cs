﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace ClusterClient
{
    static class TaskExtensions
    {
        public static async Task WaitFor(this Task task, TimeSpan timeout)
        {
            await Task.WhenAny(task, Task.Delay(timeout));
            if (!task.IsCompleted)
                throw new TimeoutException();
        }

        public static async Task<T> WaitFor<T>(this Task<T> task, TimeSpan timeout)
        {
            await Task.WhenAny(task, Task.Delay(timeout));
            if (!task.IsCompleted)
                throw new TimeoutException();
            return await task;
        }

        public static async Task<T> WaitForResult<T>(this ISet<Task<T>> awaitedTasks, TimeSpan timeout)
        {
            var finishTime = DateTime.Now + timeout;

            var innerExceptions = new List<Exception>();
            while (awaitedTasks.Count > 0)
            {
                var firstEnded = await Task.WhenAny(awaitedTasks).WaitFor(finishTime.Subtract(DateTime.Now));
                if (firstEnded.Status == TaskStatus.RanToCompletion)
                    return await firstEnded;
                innerExceptions.Add(firstEnded.Exception);
                awaitedTasks.Remove(firstEnded);
            }
            throw new AggregateException("All tasks thrown exceptions", innerExceptions);
        }
    }
}