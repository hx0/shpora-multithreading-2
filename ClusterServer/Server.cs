﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;

namespace ClusterServer
{
    class Server
    {
        private readonly ServerArguments args;
        private int lastQueryId;
        private readonly BlockingCollection<Tuple<int, HttpListenerContext>> queryQueue;
        private readonly HashSet<int> cancelledQueryIds;

        private readonly Random random = new Random();

        public Server(ServerArguments args)
        {
            this.args = args;
            lastQueryId = 0;
            queryQueue = new BlockingCollection<Tuple<int, HttpListenerContext>>();
            cancelledQueryIds = new HashSet<int>();
        }

        public void Start()
        {
            // Предполагаем, что задачи, которые выполняет сервер (например, какие-то вычисления) уже распараллелены и
            // занимают все его процессоры. Поэтому потока в сервере только два: поток, выполняющий требуемые задачи
            // и поток, обрабатывающий управляющие сообщения (например, cancel).
            Task.Run(() =>
            {
                try
                {
                    ProcessQueries();
                }
                catch (Exception e)
                {
                    Log.Error(e);
                    Console.WriteLine($"Request processor error: {e.Message}");
                }
            });

            var listener = new HttpListener
            {
                Prefixes = { $"http://+:{args.Port}/{args.MethodName}/" }
            };
            listener.Start();
            Console.WriteLine("Server started listening prefixes: {0}", string.Join(";", listener.Prefixes));

            while (true)
            {
                try
                {
                    HandleRequest(listener.GetContext());
                }
                catch (Exception e)
                {
                    Log.Error(e);
                    Console.WriteLine($"Callback handler error: {e.Message}");
                }
            }
        }

        private void HandleRequest(HttpListenerContext context)
        {
            var query = context.Request.QueryString["query"];
            var action = context.Request.QueryString["action"];

            if (query != null)
            {
                HandleWorkQuery(context);
                return;
            }
            try
            {
                if (action == "cancel")
                    HandleCancel(context);
                else
                if (action == "get_workload")
                    HandleGetWorkload(context);
                else
                    throw new ArgumentException("Invalid request");
            }
            finally 
            {
                context.Response.Close();
            }
        }

        private void HandleWorkQuery(HttpListenerContext context)
        {
            if (SimulateServerProblems(context))
                return;

            lastQueryId++;

            var bytes = Encoding.UTF8.GetBytes(lastQueryId + "\n");
            context.Response.OutputStream.Write(bytes, 0, bytes.Length);

            queryQueue.Add(Tuple.Create(lastQueryId, context));
        }

        private void HandleCancel(HttpListenerContext context)
        {
            Console.WriteLine("Cancelling {0}", context.Request.QueryString["id"]);

            lock (cancelledQueryIds)
                cancelledQueryIds.Add(int.Parse(context.Request.QueryString["id"]));
        }

        private void HandleGetWorkload(HttpListenerContext context)
        {
            Console.WriteLine("Returning workload report");

            int workload;
            lock (cancelledQueryIds)
                workload = queryQueue.Count(item => !cancelledQueryIds.Contains(item.Item1));
            var bytes = Encoding.UTF8.GetBytes(workload.ToString());
            context.Response.OutputStream.Write(bytes, 0, bytes.Length);
        }

        private void ProcessQueries()
        {
            while (true)
            {
                var item = queryQueue.Take();
                var queryId = item.Item1;
                var context = item.Item2;

                try
                {
                    lock (cancelledQueryIds)
                        if (cancelledQueryIds.Contains(queryId))
                        {
                            Console.WriteLine("Thread #{0} skips request #{1}",
                                Thread.CurrentThread.ManagedThreadId, queryId);
                            context.Response.Close();
                            continue;
                        }

                    Console.WriteLine("Thread #{0} fetched request #{1} at {2}",
                        Thread.CurrentThread.ManagedThreadId, queryId, DateTime.Now.TimeOfDay);

                    var query = context.Request.QueryString["query"];
                    var resultBytes = DoHeavyWork(query);
                    context.Response.OutputStream.Write(resultBytes, 0, resultBytes.Length);
                    context.Response.Close();

                    Console.WriteLine("Thread #{0} sent response #{1} at {2}",
                        Thread.CurrentThread.ManagedThreadId, queryId, DateTime.Now.TimeOfDay);
                }
                catch (HttpListenerException) { }
            }
        }

        private bool SimulateServerProblems(HttpListenerContext context)
        {
            if (!args.SimulateServerProblems || random.Next(6) != 0)
                return false;

            Console.WriteLine("Simulating server problems");
            context.Response.OutputStream.Close();
            return true;
        }

        private void SimulateStopTheWorld()
        {
            if (!args.SimulateStopTheWorld || random.Next(12) != 0)
                return;

            Console.WriteLine("Simulating stop-the-world because of GC");
            Thread.Sleep(args.MethodDuration * 10);
        }

        private byte[] DoHeavyWork(string query)
        {
            SimulateStopTheWorld();
            Thread.Sleep(args.MethodDuration);

            return GetBase64HashBytes(query, Encoding.UTF8);
        }

        private static byte[] GetBase64HashBytes(string query, Encoding encoding)
        {
            using (var hasher = new HMACMD5(Key))
            {
                var hash = Convert.ToBase64String(hasher.ComputeHash(encoding.GetBytes(query)));
                return encoding.GetBytes(hash);
            }
        }

        private static readonly byte[] Key = Encoding.UTF8.GetBytes("Контур.Шпора");

        private static readonly ILog Log = LogManager.GetLogger(typeof(Server));
    }
}
