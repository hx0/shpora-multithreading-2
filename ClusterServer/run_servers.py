import os
import subprocess
from urllib.parse import urlparse


EXECUTABLE = os.path.join(os.path.dirname(__file__), 'bin', 'Debug', 'ClusterServer.exe')
assert os.path.isfile(EXECUTABLE)
ADDRESS_FILE = os.path.join(os.path.dirname(__file__), '..', 'ClusterClient', 'ServerAddresses.txt')
assert os.path.isfile(ADDRESS_FILE)

params = []
with open(ADDRESS_FILE) as f:
	for line in f:
		line = line.rstrip()
		parsed = urlparse(line)
		print('Address:', line)
		assert parsed.scheme == 'http'
		params.append((parsed.port, parsed.path[1:].rstrip('/')))

processes = []
try:
	for port, path in params:
		processes.append(subprocess.Popen([EXECUTABLE, '-d', '150' if port % 2 == 1 else '350', '-n', path, '-p', str(port)]))
	
	for item in processes:
		print('Returned', item.wait())
except KeyboardInterrupt:
	pass
finally:
	print('[*] Terminating...')
	for item in processes:
		item.kill()