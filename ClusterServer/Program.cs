﻿using System;
using log4net;
using log4net.Config;

namespace ClusterServer
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            XmlConfigurator.Configure();

            try
            {
                ServerArguments parsedArgs;
                if (!ServerArguments.TryGetArguments(args, out parsedArgs))
                    return;

                new Server(parsedArgs).Start();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Server error: {e.Message}");
                Log.Fatal(e);
            }
        }

        private static readonly ILog Log = LogManager.GetLogger(typeof(Program));
    }
}
